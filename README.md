# Easepick

Easepick is a date picker. Tiny size, no dependencies. This module
allows integration of easepick into Drupal Forms.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/easepick).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/easepick).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Augusto Fagioli - [afagioli4](https://www.drupal.org/u/afagioli)
