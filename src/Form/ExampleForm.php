<?php

namespace Drupal\easepick\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A Sample form with dates borrowed from Bee Hotel module.
 */
class ExampleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'easepick_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $today = new \DateTime();
    $tomorrow = clone($today);
    $tomorrow->modify('+1 day');

    $one_hour_later = clone($today);
    $one_hour_later->modify('+1 hour');

    // Check in.
    $form['checkin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Check in'),
      '#required' => TRUE,
      '#weight' => -1000,
    ];

    // People (occupants).
    $max_occupancy = 3;
    $options = [];
    for ($o = 1; $o <= $max_occupancy; $o++) {
      $options[$o] = $o . " " . $this->t("guests");
    }

    $form['guests'] = [
      '#type' => 'select',
      '#title' => $this->t('Guests'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Search'),
    ];

    // Give easepick features to this form.
    $form['easepick'] = [
      '#value' => TRUE,
      '#type' => 'value',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
