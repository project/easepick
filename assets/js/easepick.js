/**
 * @file
 * Provides easepick features for a  date field.
 *
 * This file is loaded when a your form has the $form['easepick']['#value'] as TRUE.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.easepick = {
    attach: function (context, settings) {
      const picker = new easepick.create({
        element: document.getElementById('edit-checkin'),
        css: [
          'https://cdn.jsdelivr.net/npm/@easepick/bundle@1.2.1/dist/index.css',
        ],
      });
    }
  }
} (jQuery, Drupal, drupalSettings));
